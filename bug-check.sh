#!/bin/bash

# bug-check -- check for software being packaged or requested

# This script is in the PUBLIC DOMAIN.
# Authors: Siddhesh Rane <siddheshrane@disroot.org>
# Adapted from wnpp-check, by David Paleino <d.paleino@gmail.com>

# Improvements from wnpp-check
# This script maintains a local cache of the wnpp pages and checks from the cache first before downloading from internet
# this saves latency as well as bandwidth
# It also uses the bugs.debian.org cgi script to request for bugs which are sometimes not available in wnpp page ( mostly because they are slightly old)
#All network accesses happen in parallel
set -e

CURLORWGET=""
GETCOMMAND=""
GETHEADER=""
EXACT=0
PROGNAME=${0##*/}

usage () { echo \
"Usage: $PROGNAME <package name> [...]
  -h,--help          Show this help message
  -v,--version       Show a version message

  Check whether a package is listed as being packaged (ITPed) or has an
  outstanding request for packaging (RFP) on the WNPP website
  https://www.debian.org/devel/wnpp/"
}

version () { echo \
"This is $PROGNAME, from the Debian devscripts package, version ###VERSION###
This script is in the PUBLIC DOMAIN.
Authors: Siddhesh Rane <siddheshrane@disroot.org>
Adapted from wnpp-check, by David Paleino <d.paleino@gmail.com>
"
}

TEMP=$(getopt -n "$PROGNAME" -o 'hve' \
	      -l 'help,version,exact' \
	      -- "$@") || (rc=$?; usage >&2; exit $rc)

eval set -- "$TEMP"

while true
do
    case "$1" in
	-h|--help) usage; exit 0 ;;
	-v|--version) version; exit 0 ;;
	-e|--exact) EXACT=1 ;;
	--) shift; break ;;
    esac
    shift
done

if [ -z "$1" ]; then
    usage
    exit 1
fi

PACKAGES="$@"

if command -v wget >/dev/null 2>&1; then
    CURLORWGET="wget"
    GETCOMMAND="wget -q -O"
    GETHEADER="--header="
elif command -v curl >/dev/null 2>&1; then
    CURLORWGET="curl"
    GETCOMMAND="curl -qfs -o"
    GETHEADER="--header "
else
    echo "$PROGNAME: need either the wget or curl package installed to run this" >&2
    exit 1
fi

CACHE=~/.cache/wnppcheck # do not quote ~
WNPP=$CACHE/wnpp

# check for cache folder in ~/.cache
if [ ! -d $CACHE ]
then
	# Creating cache folder
	mkdir -p $CACHE
	echo "" > $WNPP
fi

# trap "rm -f '$WNPP' '$WNPPTMP'" 0 1 2 3 7 10 13 15
# trap "rm -f '$WNPP' '$WNPPTMP' '$WNPP_PACKAGES'" \
#  0 1 2 3 7 10 13 15

# Here's a really sly sed script.  Rather than first grepping for
# matching lines and then processing them, this attempts to sed
# every line; those which succeed execute the 'p' command, those
# which don't skip over it to the label 'd'

#ETag is an http header which tells if a document has been modified or not.
#Since wnpp pages are static they have a ETag which we store and send every time we make a requested
#If our ETag matches the server will not send the whole file again because we already have it, thus reducing network usage.
retrieve_etag () {
	#TODO: ETag support Not Yet Implemented
	grep "ETag:" $1 | sed -r 's/.*ETag:(.*)/\1/' >> $2
}

download_wnpp_itp () {
	WNPP_ITP=`mktemp -t bug-check.XXXXXX`
	OUTPUT=$1
	$GETCOMMAND $WNPP_ITP http://www.debian.org/devel/wnpp/being_packaged || \
	{ echo "$PROGNAME: $CURLORWGET http://www.debian.org/devel/wnpp/being_packaged failed." >&2; rm -f $WNPP_ITP ; exit 1; }
	sed -ne 's/.*<li><a href="https\?:\/\/bugs.debian.org\/\([0-9]*\)">\([^:<]*\)[: ]*\([^<]*\)<\/a>.*/ITP \1 \2 \3/; T d; p; : d' $WNPP_ITP > $OUTPUT
	rm -f $WNPP_ITP
}

download_wnpp_rfp () {
	WNPP_RFP=`mktemp -t bug-check.XXXXXX`
	OUTPUT=$1
	$GETCOMMAND $WNPP_RFP http://www.debian.org/devel/wnpp/requested || \
	{ echo "$PROGNAME: $CURLORWGET http://www.debian.org/devel/wnpp/requested failed." >&2; rm -f $WNPP_RFP ; exit 1; }
	sed -ne 's/.*<li><a href="https\?:\/\/bugs.debian.org\/\([0-9]*\)">\([^:<]*\)[: ]*\([^<]*\)<\/a>.*/RFP \1 \2 \3/; T d; p; : d' $WNPP_RFP > $OUTPUT
	rm -f $WNPP_RFP
}

download_bugs () {
	OUTPUT=$1
	shift
	for pkg in $@
	do
		PKGS=$PKGS'include=subject%3A'$pkg';'
	done
	BUG_TMP=`mktemp -t bug-check.XXXXXX`
	$GETCOMMAND $BUG_TMP "https://bugs.debian.org/cgi-bin/pkgreport.cgi?dist=unstable;$PKGS""package=wnpp"
	sed -nre 's/.*bug=([0-9]+)">(ITP|RFP)\s*:\s*([^ \t]+)[- ]*(.*)<\/a>/\2 \1 \3 \4/; T d; p; : d' $BUG_TMP > $OUTPUT
	rm -f $BUG_TMP
}

search_and_print () {
	NOT_FOUND=
	for pkg in $@
	do
		search=$(echo "$pkg" | sed 's/^\(libjs\|node\)-//')
		if ! grep --color=auto -P "^(ITP|RFP) \d+ (libjs|node)-\K$search(?= )" $WNPP 2>/dev/null
		then
			NOT_FOUND="$NOT_FOUND $pkg"
		fi
	done
}

#do a search in local db first for 
search_and_print $@
#now we have a list of missing packages in NOT_FOUND
if [ -n "$NOT_FOUND" ]
then
	#do a bug search
	BUGS_DB=`mktemp -t bug-check.XXXXXX`
	WNPP_ITP_DB=`mktemp -t bug-check.XXXXXX`
	WNPP_RFP_DB=`mktemp -t bug-check.XXXXXX`
	WNPPTMP=`mktemp -t bug-check.XXXXXX`
	download_bugs $BUGS_DB $NOT_FOUND & 
	download_wnpp_itp $WNPP_ITP_DB &
	download_wnpp_rfp $WNPP_RFP_DB &
	wait
	cat $BUGS_DB $WNPP_ITP_DB $WNPP_RFP_DB >> $WNPP
	sort -u -k1,1 -k2,2r $WNPP -o $WNPP
	rm -f $BUGS_DB
	rm -f $WNPP_ITP_DB
	rm -f $WNPP_RFP_DB
	rm -f $WNPPTMP
fi
#we have an up to date db now. Search again locally but only for NOT_FOUND ones
search_and_print $NOT_FOUND
exit 0
